package com.fhs.easycloud.test.controller;

import com.alibaba.nacos.client.utils.JSONUtils;
import com.fhs.easycloud.test.pojo.UserDto;
import com.fhs.easycloud.test.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("test")
public class TestController {
    @Autowired
    private UserService userService;

    @GetMapping("listArray")
    public void listArray() throws IOException {
        System.out.println(JSONUtils.serializeObject(userService.listByIds(new String[]{"1","2"})));
    }

    @GetMapping("listList")
    public void listList() throws IOException {
        ArrayList list = new ArrayList();
        list.add("1");
        list.add("2");
        System.out.println(JSONUtils.serializeObject(userService.listByIdsx(list)));
    }

    @GetMapping("updateBatch")
    public void updateBatch() throws IOException {
        UserDto dto = new UserDto();
        dto.setAge(10);
        dto.setUserName("wanglei");
        dto.setBirthday(new Date());
        ArrayList list = new ArrayList();
        list.add(dto);
        System.out.println(userService.updateBatch(list));
    }

    @GetMapping("manyArgs")
    public void manyArgs() throws IOException {
        System.out.println(JSONUtils.serializeObject(userService.manyArgs("1","2")));
    }
    @GetMapping("testMap")
    public void testMap() throws IOException {
        Map<String,Object> map = new HashMap<>();
        map.put("1","1");
        map.put("2",true);
        map.put("3",100l);
        map.put("4",100d);
        map.put("5",new  Date());
        System.out.println(JSONUtils.serializeObject(userService.testMap(map)));
    }

}
