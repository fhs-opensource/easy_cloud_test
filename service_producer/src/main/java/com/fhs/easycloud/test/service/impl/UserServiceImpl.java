package com.fhs.easycloud.test.service.impl;

import com.alibaba.nacos.client.utils.JSONUtils;
import com.fhs.easycloud.test.pojo.UserDto;
import com.fhs.easycloud.test.service.UserService;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;

@Service
public class UserServiceImpl implements UserService {
    @Override
    public List<UserDto> listByIds(String[] ids) {
        try {
            System.out.println(JSONUtils.serializeObject(ids));
        } catch (IOException e) {
            e.printStackTrace();
        }
        UserDto dto = new UserDto();
        dto.setAge(10);
        dto.setUserName("wanglei");
        dto.setBirthday(new Date());
        ArrayList list = new ArrayList();
        list.add(dto);
        return list;
    }

    @Override
    public List<UserDto> listByIdsx(List<String> ids) {
        try {
            System.out.println(JSONUtils.serializeObject(ids));
        } catch (IOException e) {
            e.printStackTrace();
        }
        UserDto dto = new UserDto();
        dto.setAge(10);
        dto.setUserName("wanglei");
        dto.setBirthday(new Date());
        ArrayList list = new ArrayList();
        list.add(dto);
        return list;
    }

    @Override
    public int updateBatch(List<UserDto> users) {
        try {
            System.out.println(JSONUtils.serializeObject(users));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 10;
    }

    @Override
    public int testMap(Map<String, Object> imMap) {
        try {
            System.out.println(JSONUtils.serializeObject(imMap));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 10;
    }

    @Override
    public UserDto manyArgs(String a, String b) {
        System.out.println(a + "--" + b);
        UserDto dto = new UserDto();
        dto.setAge(10);
        dto.setUserName("wanglei");
        dto.setBirthday(new Date());
        return dto;
    }
}
