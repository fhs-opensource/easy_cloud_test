package com.fhs.easycloud.test.service;

import com.fhs.easycloud.anno.CloudApi;
import com.fhs.easycloud.anno.CloudMethod;
import com.fhs.easycloud.test.pojo.UserDto;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@CloudApi(serviceName="producer")
public interface UserService {

    @CloudMethod
    List<UserDto> listByIds(String[] ids);

    @CloudMethod
    List<UserDto> listByIdsx(List<String> ids);

    @CloudMethod
    int updateBatch(List<UserDto> users);

    @CloudMethod
    int testMap(Map<String,Object> imMap);

    @CloudMethod
    UserDto manyArgs(String a,String b);

}
