package com.fhs.easycloud.test.pojo;

import lombok.Data;

import java.util.Date;
@Data
public class UserDto {
    private String userName;
    private Integer age;
    private Date birthday;
}
